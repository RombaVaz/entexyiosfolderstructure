//
//  File.swift
//  Smass-Cars-IOS
//
//  Created by x.one on 13.09.22.
//

Application
  AppDelegate
  SceneDelegate
Resources
  Storyboards
  Nibs
  Fonts
  Assets
  Localization
  Plist
Helpers
  Constants
  Extensions
  Stylesheet
  Bridging
  Library // Loggers, specific services...
UI 
  Cells
    TableViewCells
    CollectionViewCells
  Coordinators // Views management
  General // Custom textfield, picker
  FeatureA
    A.swift
    AView.swift
    AController.swift
Core 
  Entities
  Managers
  Repository
  Routers
  Storages
  Clients
  Presenters/ViewModels
  Interactors
  Utilites
Tests
  Unit
  Snapshot
  Integration
Frameworks
Products // .app, .xctest, .appex ...

